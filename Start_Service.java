
//Starts a service (task to be accomplished in the background, without UI)
//The class employing the snippet code must implement ServiceConnection
Intent iServ = new Intent();
iServ.setClass(getBaseContext(), ServiceName.class); //TODO Replace 'ServiceName' with the class name for your Service
bindService(iServ, this, BIND_AUTO_CREATE);
startService(iServ);	